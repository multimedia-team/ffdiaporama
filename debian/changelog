ffdiaporama (2.1+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ James Cowgill ]
  * New upstream release. (Closes: #731777)
  * Remove background resources. These resources are not released under a DFSG
    free license.

  * debian/compat:
    - Use debhelper compat 11.
  * debian/control
    - Set maintainer to debian-multimedia@l.d.o.
    - Build depend on qtbase5-dev instead of qt5-default.
    - Drop dependency on ffmpeg.
    - Drop shlibs:Depends from ffdiaporama-data.
    - Bump standards version to 4.1.3.
    - Set Rules-Requires-Root: no.
    - Mark ffdiaporama-data as Multi-Arch foreign.
  * debian/copyright:
    - Update for 2.1.
  * debian/ffdiaporama-data.links:
    - Add a dummy background to prevent segfaults.
  * debian/patches:
    - Rename and refresh 01_fix-hardening-flags.patch.
    - Update and split up ffmpeg patches.
    - Add patch to fix uninitialized variable valgrind warnings.
    - Add patch to make LibAvLogCallback thread safe.
    - Add patch to enable rsc subproject.
    - Add patch to fix 64-bit architecture detection.
    - Add patch to prevent phoning home for updates.
    - Add patch to fix FTBFS with FFmpeg 4.0. (Closes: #888354)
  * debian/rules:
    - Remove DH_VERBOSE comment from debian/rules.
    - Install upstream changelog.
    - Export QT_SELECT in debian/rules.
  * debian/watch:
    - Update debian/watch to new source location.

  [ Rico Tzschichholz ]
  * Update fix_hardening_flags.patch
  * Add ffmpeg_2.8.patch for ffmpeg 2.8+ support.
  * Install application icon. (LP: #1280820)
  * Use ffmpeg >= 2.8 and QT5. (Closes: #873191, #874874)

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/control: Set Vcs-* to salsa.debian.org

 -- James Cowgill <jcowgill@debian.org>  Thu, 22 Feb 2018 21:40:49 +0000

ffdiaporama (1.5-5) unstable; urgency=medium

  * Team upload.

  [ Andreas Cadhalpun ]
  * Fix build with ffmpeg 3.0. (Closes: #803810)

  [ Sebastian Ramacher ]
  * debian/control:
    - Update Standards-Version to 3.9.7.
    - Update Vcs-*.
  * debian/rules:
    - Remove unnecessary override.
    - Build with --parallel.

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 14 Mar 2016 21:17:15 +0100

ffdiaporama (1.5-4) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.
  * debian/control: Bump Standards-Version.

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 12 May 2014 01:36:54 +0200

ffdiaporama (1.5-3) experimental; urgency=low

  * Team upload.
  * Compile against libav10 (Closes: #739221)

 -- Reinhard Tartler <siretart@tauware.de>  Mon, 24 Mar 2014 19:21:00 -0400

ffdiaporama (1.5-2) unstable; urgency=low

  * Upload to unstable.

  [ Fabrice Coutadeur ]
  * debian/control: Drop dependency on -extra packages. It should work without
  * libav-0.9.patch: adapt options passed to avconv

  [ Sebastian Ramacher ]
  * debian/control: Build-Depend on debhelper >= 9 for compat level 9.

 -- Fabrice Coutadeur <coutadeurf@gmail.com>  Tue, 10 Sep 2013 22:18:39 +0200

ffdiaporama (1.5-1) experimental; urgency=low

  [ Fabrice Coutadeur ]
  * New upstream version:
    - New graphical filters
    - New speed curves
    - New transitions
    - Adapted text margins
    - All graphical filters are now applied in a progressive way
  * debian/control:
    - Added b-d on libqimageblitz-dev
    - Replaced dependency on ffmpeg with libav-tools
    - Added dependency on avcodec-extra libs

  [ Alessio Treglia ]
  * Bump Standards.

 -- Fabrice Coutadeur <coutadeurf@gmail.com>  Thu, 31 Jan 2013 09:10:07 +0000

ffdiaporama (1.3.1-1) unstable; urgency=low

  [ Fabrice Coutadeur ]
  * New upstream bugfix-only version.
  * Set compat level to 9 to get hardening flags automatically, the hardening
    flags are added to the corresponding variable in debian/rules as well.
  * Fix common.pri file to get LDFLAGS and CFLAGS from env, this allows the
    compilation to use the hardening flags and get rid of the missing
    hardening flags warnings when running lintian.

  [ Alessio Treglia ]
  * Fix Vcs-Browser field.

 -- Fabrice Coutadeur <coutadeurf@gmail.com>  Mon, 16 Jul 2012 20:01:03 +0200

ffdiaporama (1.3-1) unstable; urgency=low

  * New upstream version
  * fix_ftbs_gcc4.7.patch: dropped as it has been adopted upstream
  * fix-ftbfs-libav-0.7.patch: fix FTBFS with libav 0.7 because of missing
    headers that has been added in libav 0.8

 -- Fabrice Coutadeur <coutadeurf@gmail.com>  Wed, 30 May 2012 06:46:55 +0200

ffdiaporama (1.2.1-1) unstable; urgency=low

  * Initial release (Closes: #669201)

 -- Fabrice Coutadeur <coutadeurf@gmail.com>  Mon, 16 Apr 2012 06:59:32 +0200
